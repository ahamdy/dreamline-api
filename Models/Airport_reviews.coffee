mongoose = require 'mongoose'
Schema = mongoose.Schema
config = require '../config/conf.coffee'

class MongoDB

  constructor: (queryinfo)->
    #read mongo config from file
    mongo = config.Mongodb
    uri = "mongodb://#{mongo.host}:#{mongo.port}/#{mongo.db_name}"
    #initiate db connection
    @db = mongoose.createConnection uri
    #create a simple model to query mongodb
    mschema = new Schema({
      airport_name: 
        type: String,
        min: 5,
        max: 100,
      })
    @airport_reviews = @db.model('airport_reviews', mschema, 'airport_reviews')
    #assign query parameters
    @queryinfo = queryinfo

  exec_aggregate: (callback)->
    #execute aggreagation call to mongodb
    @airport_reviews.aggregate  @queryinfo, (err, data) ->
      mongoose.connection.close()
      if !err
        return callback null, data
      else
        return callback err, null

  exec_find: (airport_name, callback)->
    @airport_name = airport_name
    #execute find airport review infro from mongodb
    @airport_reviews.find({airport_name: @airport_name}, @queryinfo).lean(true).sort({date: -1}).exec (err, data) ->
      mongoose.connection.close()
      if !err
        return callback null, data
      else
        console.log 'db find error',err
        return callback err, null

module.exports = MongoDB