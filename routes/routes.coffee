Stats_Controllers = require '../Controllers/All_stats.coffee'
Airport_stat_Controllers = require '../Controllers/Airport_stats.coffee'
Airport_reviews_Controllers = require '../Controllers/Airport_reviews.coffee'

module.exports = (params) ->
  app = params.app

  #setting handlers for API
  app.get '/api/all/stats', Stats_Controllers.Get_All_Airports_Stats

  app.get '/api/:airport/stats', Airport_stat_Controllers.Get_Airport_Stats

  app.get '/api/:airport/reviews', Airport_reviews_Controllers.Get_Airport_reviews

  #setting apir 404 error handler
  app.get '*', (req,res) ->
    res.status 404
    res.json {err: 'not found'}