module.exports =
  'Mongodb':
    'host': 'localhost',
    'port': '27017',
    'db_name': 'review_db',
  'Memcached':
    'host': 'localhost',
    'port': '11211'