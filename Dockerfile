FROM ubuntu:14.04

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update 
RUN apt-get install -y memcached 
RUN ln -s /usr/bin/nodejs /usr/bin/node 
RUN mkdir /home/api/
RUN apt-get install memcached 
RUN service memcached start 
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 
RUN echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list 
RUN apt-get update 
RUN apt-get install -y mongodb-org
RUN	apt-get install -y wget 
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash - && apt-get install -y nodejs
RUN npm install -g coffee-script@1.10 
RUN npm install -g mocha@2.5.3

COPY . /home/api

WORKDIR /home/api/

RUN chmod +x mongo_start.sh

EXPOSE 8080

RUN npm install

ENTRYPOINT ./mongo_start.sh && npm test && npm start
