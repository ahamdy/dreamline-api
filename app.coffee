express = require 'express'
routes = require './routes/routes'
logger = require 'morgan'
fs = require 'fs'

app = express()

#log morgan accesslog to access.log file
accessLogStream = fs.createWriteStream __dirname + '/access.log', {flags: 'a'}
app.use(logger('combined', {stream: accessLogStream}))

#set env port from shell or to 8080
app.set('port', process.env.PORT || 8080);

#Dsiable logging while running test cases
if process.env.NODE_ENV != 'test'
  app.use(logger('dev'))
  
#pass app as argument to routes
routes({app:app})

#start listening on port
app.listen app.get('port')

if process.env.NODE_ENV != 'test'
  console.log "Express server listening on port #{app.get('port')}"

#for mocha testing purposes
module.exports = app