mdb = require '../Models/Airport_reviews.coffee'
Memcached = require 'memcached'
config = require '../config/conf.coffee'

exports.Get_All_Airports_Stats = (req, res) ->
  #read memcache config from file
  memcached = config.Memcached

  #mongodb query aggregation array
  db_aggregate  = [

    { '$match': 'airport_name': '$ne': null },
    { '$group':
      '_id': 'airport_name': '$airport_name'
      'count': '$sum': 1 },
      {'$sort': 'count': -1  }
  ]
  #start memcached connection
  memcached = new Memcached "#{memcached.host}:#{memcached.port}", {retries:0, timeout:100}

  #check if key all_stats value is already stored on memcached and return if true
  memcached.gets 'all_stats', (err, data)->
    if !err && data
      memcached.end
      res.setHeader 'Content-Type', 'application/json'
      res.status 200
      res.json data.all_stats
    else
      #if key is a cache miss fetch data from mongodb
      db = new mdb db_aggregate
      #call aggregate function
      db.exec_aggregate (err, cb)->
        if !err 
          json = []
          #prepare json output
          for value in cb
            json.push {airport_name: value._id.airport_name, review_count: value.count}
          #set key for next hit in asynchronous manner
          memcached.set 'all_stats', json, 10*60 , (err) ->
            memcached.end
          res.setHeader 'Content-Type', 'application/json'
          res.status 200
          res.json json
        else
          console.log err
          memcached.end
          res.status 500
          res.json { err: "Internal server error"}