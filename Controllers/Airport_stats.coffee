mdb = require '../Models/Airport_reviews.coffee'
Memcached = require 'memcached'
config = require '../config/conf.coffee'

exports.Get_Airport_Stats = (req, res) ->
  #read airport name from query parameters
  airport_name = req.params.airport.replace(/\$/g, "")
  #read memcached config from file
  memcached = config.Memcached

  #mongofb consturct db aggregation array
  db_aggregate  = [
    { '$match': 'airport_name': airport_name },
    { '$group':
      '_id': 
        'airport_name': '$airport_name',
        "recommended": "$recommended",
        "overall_rating": "$overall_rating",
      'count': '$sum': 1 },
    {'$sort': 'count': -1  }
  ]
  #establish memcached connection
  memcached = new Memcached "#{memcached.host}:#{memcached.port}", {retries:0, timeout:100}

  #check if key airport_name key value is already stored on memcached and return if true
  memcached.gets airport_name, (err, data)->
    if !err && data
      memcached.end
      res.setHeader 'Content-Type', 'application/json'
      res.status 200
      res.json data[airport_name]
    else
      #if key is a cache miss fetch data from mongodb    
      db = new mdb db_aggregate
      db.exec_aggregate (err, cb)->
        if !err
          if cb.length > 0
            #prepare json object
            Process_data cb,(err,json)->
              #set key for next hit in asynchronous manner
              memcached.set airport_name, json, 10*60 , (err) ->
                memcached.end          
              res.setHeader 'Content-Type', 'application/json'
              res.status 200
              res.json json
          else
            memcached.end
            res.setHeader 'Content-Type', 'application/json'
            res.status 400
            res.json { err: "Incorrect airport name"}
        else
          memcached.end
          console.log err
          res.setHeader 'Content-Type', 'application/json'
          res.status 500
          res.json {err: "a general error occured"}

  #Async function to prepare json object
  Process_data = (data, json_cb)->
    json = {}
    json.review_count = 0
    json.recommended = 0
    json.overall_rating = 0
    json.avg_correction = 0
    for value in data
      json.airport_name =  value._id.airport_name
      json.review_count = json.review_count + value.count
      json.recommended = json.recommended + value._id.recommended * value.count

      #overall_rating calculations
      if typeof value._id.overall_rating == "number"
        json.overall_rating = json.overall_rating + (value._id.overall_rating * value.count)
      else
        json.avg_correction =  json.avg_correction + value.count
    #remove non rating from review count and get overall rating 
    json.overall_rating = json.overall_rating / (json.review_count - json.avg_correction)
    delete   json.avg_correction    
    return json_cb null, json