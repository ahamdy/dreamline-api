mdb = require '../Models/Airport_reviews.coffee'
Memcached = require 'memcached'
config = require '../config/conf.coffee'

exports.Get_Airport_reviews = (req, res) ->
  #get airport name from url
  airport_name = req.params.airport.replace(/\$/g, "") 
  #get acceptable review ratio from url param cap
  review_cap = parseInt(req.query.cap) || 0
  #read memcached config from file
  memcached = config.Memcached

  fields = 
    _id: false,
    overall_rating: true,
    recommended: true,
    date: true,
    author_country: true,
    content: true
  #establish memcached connection
  memcached = new Memcached "#{memcached.host}:#{memcached.port}", {retries:0, timeout:100}
  
  #check if key airport_name+cap key value is already stored on memcached and return if true
  memcached.gets airport_name+review_cap , (err, data)->
    if !err && data
      memcached.end
      res.setHeader 'Content-Type', 'application/json'
      res.status 200
      res.json data[airport_name+review_cap]
    else
      #if key is a cache miss fetch data from mongodb
      db = new mdb fields
      db.exec_find airport_name, (err, cb)->
        if !err
          if cb.length > 0
            #if review cap is a valide number in range remove all reviews less than cap
            if review_cap in [1..10]
              filtered_reviews = []
              for record in cb
                #push acceptable reviews to var filtered_reviews
                filtered_reviews.push(record) if record.overall_rating >= review_cap
              #set key for cache hit
              memcached.set airport_name+review_cap, filtered_reviews, 10*60 , (err) ->
                memcached.end 
              res.setHeader 'Content-Type', 'application/json'
              res.status 200
              res.json filtered_reviews           
            else
              memcached.set airport_name+review_cap, cb, 10*60 , (err) ->
                memcached.end
              res.setHeader 'Content-Type', 'application/json'
              res.status 200
              res.json cb
          else
            memcached.end
            res.setHeader 'Content-Type', 'application/json'
            res.status 400
            res.json {err: "no airport data found"}        
        else
          memcached.end
          res.status 500
          res.json {err: "internal error occurred"}