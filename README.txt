Airport reviews API:

To run use the DockerFile in the repo and build the docker image using the following command whil in project directory:
"docker build -t airports_api ."

After build is finished issue the following command to run as a deamon:

"docker run -p 9999:8080 -d airports_api"

The previous command binds port 9999 to 8080, to preform a request use the following:

"curl http://localhost:9999/api/all/stats"

The service start by performing tests then starting the app, to see test results pull logs from the container, by issuing the following command:
"docker ps"
use the container id in the next command:
"docker logs <container id>"

To hook into the running container:
"docker exec -it <container id> /bin/sh"

For more information about using the API, check documents folder.

To run unit tests(server must not be started):
npm test

Technologies used: Nodejs, coffeescript, expressjs, mongodb, memcached. mocha

Author:
Abdallah Hamdy



