process.env.NODE_ENV = 'test'
should = require 'should'
assert = require 'assert'
request = require 'supertest'
winston = require 'winston'

describe 'Airport API', ->
  url = 'http://localhost:8080'

  before (done)->
    ##Start NodeJs app
    app = require '../app'
    return done()

  ##Test Not found page
  describe '404 Not Found', ->
    it 'should return status 404 not found ', (done) ->
      request(url).get('/notofund')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.status.should.be.equal(404)
        return done()

    it 'should return json error implying resource requested not found', (done) ->
      request(url).get('/notofund')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.body.err.should.equal("not found")
        return done()

  #Test /api/stats/all for multiple failure/success cases
  describe 'All Airport stats API', ->
    it 'should return status 200 success', (done) ->
      request(url).get('/api/all/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.status.should.be.equal(200)
        return done()

    it 'should return Json Object', (done) ->
      request(url).get('/api/all/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.body.should.be.type('object')
        return done() 

    it 'should return Object length larger than 0', (done) ->
      request(url).get('/api/all/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.body.length.should.be.above(0)
        return done() 

  #Test /api/[airport]/stats for multiple failure/success cases
  describe 'Airport Stats API', ->
    it 'should return http 200 success when calling a correct airport', (done) ->
      request(url).get('/api/london-heathrow-airport/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.status.should.be.equal(200)
        return done()

    it 'should return Object with 5 keys', (done) ->
      request(url).get('/api/london-heathrow-airport/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        Object.keys(res.body).length.should.be.equal(4)
        return done()        

    it 'should return http 400 when calling non-existant airport', (done) ->
      request(url).get('/api/xyz/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.status.should.be.equal(400)
        return done()

    it 'should return json error implying airport requested not found', (done) ->
      request(url).get('/api/xyz/stats')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.body.err.should.equal("Incorrect airport name")
        return done() 

  #Test /api/[airport]/reviews for multiple failure/success cases
  describe 'Airport reviews API', ->
    ##Airport in dataset/success
    it 'should return http 200 success when calling a correct airport', (done) ->
      request(url).get('/api/london-heathrow-airport/reviews')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.status.should.be.equal(200)
        return done()

    it 'should return more then 1 review object', (done) ->
      request(url).get('/api/london-heathrow-airport/reviews')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.body.length.should.be.above(0)
        return done()  

    ##Non existant airport/falure
    it 'should return http 400 when calling non-existant airport', (done) ->
      request(url).get('/api/xyz/reviews')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.status.should.be.equal(400)
        return done()

    it 'should return json error implying airport requested not found', (done) ->
      request(url).get('/api/xyz/reviews')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end (err, res) ->
        if err
          throw err
        res.body.err.should.equal("no airport data found")
        return done()          
